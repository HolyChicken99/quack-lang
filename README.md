# Brain-duck
![](https://i.postimg.cc/cCgWf9qP/Brain-Duck.png=100x20)

## Dependencies
* make 
* g++ 


```bash
make 

chmod +x MountCC 

touch main.mount
// add you quack code

quackCC main.quack
```
> examples in the example folder

# Commands for Quacking 


| Commands   | Description |
| ----------- | ----------- |
|   mount   | This command is connected to the QUACK command. When encountered during normal execution, it searches the program code in reverse looking for a matching QUACK command and begins executing again starting from the found QUACK command. When searching, it skips the instruction that is immediately before it (see MOUNT).      |
|  mOunt | Moves current memory position back one block.       |
| mouNt | Moves current memory position forward one block.|
| mouNT| Execute value in current memory block as if it were an instruction. The command executed is based on the instruction code value (for example, if the current memory block contains a 2, then the mouNt command is executed). An invalid command exits the running program. Value 3 is invalid as it would cause an infinite loop. |
|Mount |If current memory block has a 0 in it, read a single ASCII character from STDIN and store it in the current memory block. If the current memory block is not 0, then print the ASCII character that corresponds to the value in the current memory block to STDOUT.  |
|MOunt|Decrement current memory block value by 1.  |
|MOunT | Increment current memory block value by 1. |
| MOUNT | If current memory block value is 0, skip next command and resume execution after the next matching moo command. If current memory block value is not 0, then continue with next command. Note that the fact that it skips the command immediately following it has interesting ramifications for where the matching moo command really is.
|  OOOOO | Set current memory block value to 0.|
| MMMMM | If no current value in register, copy current memory block value. If there is a value in the register, then paste that value into the current memory block and clear the register. |
|MoUnT | Print value of current memory block to STDOUT as an integer. |
|MoUnt|Read an integer from STDIN and put it into the current memory block.  | 


