
#include <cstring>
#include <fstream>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>

#define COMPILER "g++"
#define FLAGS "-O3 -x c++"
#define NAME_FLAG "-o "
#define OUTPUT_EXEC "mount.out"
#define OUTPUT_CPP "mount.out.cpp"

typedef std::vector<int> mem_t;
mem_t program;
mem_t::iterator prog_pos;
FILE *output;

int quackcount(0);
int QUACKcount(0);

void quit() {
  printf("Compile error.  Invalid source code.\n");
  exit(1);
}

bool compile(int instruction, bool advance) {
  switch (instruction) {
  // quack
  case 0: {
    int level = 1;
    int num = QUACKcount + 1;
    mem_t::iterator t = prog_pos;

    t--; // skip past previous command when searching for QUACK.
    if (t != program.begin()) {
      if ((*t) == 7)
        num--;

      while (level > 0) {
        if (t == program.begin())
          break;

        t--;

        if ((*t) == 0)
          level++;
        else if ((*t) == 7) // look for QUACK
        {
          level--;
          num--;
        }
      }
    }

    if (level != 0 && advance)
      quit();
    else if (level != 0) {
      fprintf(output, "rterr();");
      break;
    }

    quackcount++;
    fprintf(output, "goto M%d;", num);
    fprintf(output, "m%d:", quackcount);
  } break;

  case 1:
    fprintf(output, "if(p==m.begin()){rterr();}else{p--;}");
    break;

  case 2:
    fprintf(output, "p++; if(p==m.end()){m.push_back(0);p=m.end();p--;}");
    break;

  case 3:

    fprintf(output, "switch(*p){");
    fprintf(output, "case 0:{");
    compile(0, false);
    fprintf(output, "}break;");
    fprintf(output, "case 1:{");
    compile(1, false);
    fprintf(output, "}break;");
    fprintf(output, "case 2:{");
    compile(2, false);
    fprintf(output, "}break;");
    fprintf(output, "case 4:{");
    compile(4, false);
    fprintf(output, "}break;");
    fprintf(output, "case 5:{");
    compile(5, false);
    fprintf(output, "}break;");
    fprintf(output, "case 6:{");
    compile(6, false);
    fprintf(output, "}break;");
    fprintf(output, "case 7:{");
    compile(7, false);
    fprintf(output, "}break;");
    fprintf(output, "case 8:{");
    compile(8, false);
    fprintf(output, "}break;");
    fprintf(output, "case 9:{");
    compile(9, false);
    fprintf(output, "}break;");
    fprintf(output, "case 10:{");
    compile(10, false);
    fprintf(output, "}break;");
    fprintf(output, "case 11:{");
    compile(11, false);
    fprintf(output, "}break;");
    fprintf(output, "default:{goto x;}};");
    break;

  case 4:
    fprintf(output, "if((*p)!=0){putchar(*p);}else{(*p)=getchar();while("
                    "getchar()!='\\n');}");
    break;

  case 5:
    fprintf(output, "(*p)--;");
    break;

  case 6:
    fprintf(output, "(*p)++;");
    break;

  case 7: {
    int level = 1;
    int num = quackcount;
    int prev = 0;
    mem_t::iterator t = prog_pos;
    t++; // have to skip past next command when looking for next moo.

    if (t != program.end()) {
      if ((*t) == 0)
        num++;

      while (level > 0) {
        prev = *t;
        t++;

        if (t == program.end())
          break;

        if ((*t) == 7) // look for QUACK command.
          level++;
        else if ((*t) == 0) // look for QUACK command.
        {
          if (prev == 7)
            level--;

          level--;
          num++;
        }

        if (level == 0)
          break;
      }
    }
    if (advance && level != 0)
      quit();
    else if (level != 0) {
      fprintf(output, "rterr();");
      break;
    }

    QUACKcount++;
    fprintf(output, "M%d:", QUACKcount);
    fprintf(output, "if(!(*p))goto m%d;", num);
  } break;

  case 8:
    fprintf(output, "(*p)=0;");
    break;

  case 9:
    fprintf(output, "if(h){(*p)=r;}else{r=(*p);}h=!h;");
    break;

  case 10:
    fprintf(output, "printf(\"%%d\\n\",*p);");
    break;

  case 11:
    fprintf(output, "{char b[100];int c=0;");
    fprintf(output, "while(c<sizeof(b)-1){b[c]=getchar();c++;b[c]=0;if(b[c-1]=="
                    "'\\n')break;}");
    fprintf(output, "if(c==sizeof(b))while(getchar()!='\\n');(*p)=atoi(b);}");
    break;

  // bad stuff
  default:
    return false;
  };

  if (advance)
    prog_pos++;

  return true;
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("QUACK QUACK !! , you entered quack ... I mean nothing!", argv[0]);
    exit(1);
  }

  // FILE* f = fopen( argv[1], "rb" );

  std::ifstream f(argv[1], std::ios::in | std::ios::binary);

  if (!f) {
    printf("Cannot open source file [%s].\n", argv[1]);
    exit(1);
  }

  char buf[5];       // character buffer to input the quacks
  memset(buf, 0, 5); // sets the buffer to 0
  int pos = 0;
  int quakcs = 0;

  while (!f.eof()) {

    // int found = 0;
    // print(buf);
    f.read(buf, 5);
    std::string buffer(buf);

    if (buffer == "mount") {
      program.push_back(0);
    } else if (buffer == "mOunt") {

      program.push_back(1);
    } else if (buffer == "mouNt") {
      program.push_back(2);
    } else if (buffer == "mouNT") {
      program.push_back(3);
    } else if (buffer == "Mount") {
      program.push_back(4);
    } else if (buffer == "MOunt") {
      program.push_back(5);
    } else if (buffer == "MOunT") {
      program.push_back(6);
    }

    else if (buffer == "MOUNT") {
      program.push_back(7);
    } else if (buffer == "OOOOO") {
      program.push_back(8);
    }

    else if (buffer == "MMMMM") {
      program.push_back(9);
    } else if (buffer == "MoUnT") {
      program.push_back(10);
    } else if (buffer == "moUnt") {
      program.push_back(11);
    }

    memset(buf, 0, 5);

    //
  }

  f.close();

  printf("Compiling [%s]...\n", argv[1]);

  output = fopen("quack.out.cpp", "wb");
  fprintf(output, "#include <stdio.h>\n");
  fprintf(output, "#include <stdlib.h>\n");
  fprintf(output, "#include <vector>\n");
  fprintf(output, "#include <cstring>\n");
  fprintf(output, "typedef std::vector<int> t_;t_ m;t_::iterator p;\n");
  fprintf(output, "bool h;int r;\n");
  fprintf(output, "void rterr(){puts(\"Runtime error.\\n\");}\n");
  fprintf(output, "int main(int a,char** v){\n");
  fprintf(output, "m.push_back(0);p=m.begin();h=false;\n");
  fprintf(output, "if(a > 1){int i = 0; char temp = v[1][i]; while(temp != "
                  "0){m.push_back(temp); i++; temp = v[1][i];} m.push_back(0); "
                  "p=m.end();}\n");

  prog_pos = program.begin();
  while (prog_pos != program.end())
    if (!compile(*prog_pos, true)) {
      printf("ERROR!\n");
      break;
    }

  fprintf(output, "x:return(0);}\n");
  fclose(output);

  printf("C++ source code: mount.out.cpp\n");

#ifdef COMPILER
  std::string path((const char *)COMPILER);
  path.append(" ");
  path.append((const char *)NAME_FLAG);
  path.append(" ");
  path.append((const char *)OUTPUT_EXEC);
  path.append(" ");
  path.append((const char *)FLAGS);
  path.append(" ");
  path.append((const char *)OUTPUT_CPP);

  if (system(path.c_str()))
    printf("\n\nCould not compile.  Possible causes:  C++ compiler is not "
           "installed, not in path, or not named '%s' or there is a bug in "
           "this compiler.\n\n",
           COMPILER);
  else
    printf("Executable created: mount.out\n");
#endif

  return 0;
}
